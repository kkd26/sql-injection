#!/bin/bash

source ./init_functions.sh

query=$(get-query-string "$1")

echo "$query"

run-query "$query"
