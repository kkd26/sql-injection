#!/bin/bash

run-query() {
  local result=$(java -jar hsqldb/lib/sqltool.jar --rcfile=db1a.rc --sql="$1" db1a 2> out.log)
  echo "$result"
}
