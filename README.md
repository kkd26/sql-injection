# Usage

After cloning run `source ./init.sh`.

I provided the IMDb movies, actors relational database which you can explore by running:

```bash
db
```

or

```bash
run-query "select * from movies limit 5;"
```

Check `out.log` file for more detailed log and errors.

## SQL injection

You can play around with SQL injection by running:

```bash
./make-query-from-actor.sh "Ben Affleck"
```

The produced output is:

```text
TITLE           YEAR
--------------  ----
State of Play   2009
The Town        2010
Argo            2012
The Accountant  2016
Gone Girl       2014
```

After modification you can run:

```bash
./make-query-from-actor.sh "' drop table movies cascade; drop table people cascade;--"
```

which drops two tables - `movies` and `people`.

## Secure SQL injection

There is also another script, which prevents SQL injection - `make-query-from-actor-secure.sh`. The usage in the same as above

## Restore database

You can restore your database at anytime by running:

```bash
./reset.sh
```

## Hyper SQL installation

You can unpack hyper sql if you don't have one by running

```bash
tar zxf hsqldb.tar.gz
```
