#!/bin/bash

source ./init_functions.sh

input=$(echo $1 | sed s/\'/\'\'/g)

query=$(get-query-string "$input")

echo "$query"

run-query "$query"
